//#include <signal.h>
#include <string>
#include <cpprest/http_listener.h>

#include "server.hpp"
#include "usr_interrupt_handler.hpp"

#include "GetPot"

using namespace std;
using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;




int main(int argc, char** argv)
{

    // ll: read parameters
    GetPot g1(argc, argv);
    std::string filename = g1("filename", "Options.txt");
    GetPot g2 (filename.c_str());

    // ll: PARAMETERS MAP (it works like a python dictionary)
    std::map<std::string, std::string> parameters;
    
    // LL: loops parameters
    parameters["nloops"] =  std::to_string(g2("nloops",10)); //int
    parameters["tolM"]   =  std::to_string(g2("tolM",1e-2)); //double
    parameters["errM"]   =  std::to_string(g2("errM",std::stod(parameters["tolM"])+1));//double

    // LL: adaptation paramters
    parameters["adp_tool"]     =  g2("adp_tool","mmg"); //string
    parameters["toll"]         =  std::to_string(g2("adp_toll",10)); //double
    parameters["hmin"]         =  std::to_string(g2("hmin",1.5e-4)); //double
    parameters["hmax"]         =  std::to_string(g2("hmax",20)); //double
    parameters["nmax"]         =  std::to_string(g2("nmax",20000)); //int
    parameters["limit_grad"]   =  std::to_string(g2("limit_grad",0)); //bool
    parameters["iso"]          =  std::to_string(g2("iso",0)); //bool
   
    // LL: object and texture files name
    parameters["object_name"]  =  g2("objname","everest"); //string
    parameters["texture_name"] =  g2("texname"," ");//string
    parameters["textured"]     =  std::to_string(g2("textured",0)); //bool
  
    // LL: screend poisson parameters
    parameters["alpha"]    = std::to_string(g2("alpha",10.0)); //double
    parameters["eps"]      = std::to_string(g2("eps",1.)); //double
    parameters["gamma"]    = std::to_string(g2("gamma",0.0)); //double

    // LL: domain_parameters
    parameters["dom_x"]   = std::to_string(g2("dom_x",40)); //int
    parameters["dom_y"]   = std::to_string(g2("dom_y",40)); //int
    parameters["dom_z"]   = std::to_string(g2("dom_z",40)); //int
    parameters["radius_marking"] =  std::to_string(g2("radius", 1.0)); //float
    parameters["domain_scale"]   =  std::to_string(g2("domain_added_cells", 0.)); //float
    parameters["max_edge"]       =  std::to_string(g2("max_edge", 15.)); //float

    // LL: post processing parameters
    parameters["smooth_it"]      =  std::to_string(g2("smoooth_it",3)); //int  

    // LL: server
    auto g_httpServer = make_unique<Server>();
    std::cout << "Starting Server" << std::endl;
   
    // LL: set parameters in the server
    g_httpServer->set_parameters_map(parameters);

    // LL: necessary to handle user input interrupt
    cfx::InterruptHandler::hookSIGINT();


   try
   {
      g_httpServer->open().wait();
      std::cout << "Server listening at: " << g_httpServer->getEndpoint() << std::endl;

      cfx::InterruptHandler::waitForUserInterrupt();

      g_httpServer->close().wait();
      std::cout << "Shutting Down Server" << std::endl;
      
   }
   catch (exception const & e)
   {
      std::cout << "--- ERROR DETECTED ---" << std::endl;
      std::cout << e.what() << std::endl;
   }
 
   return 0;
}
#ifndef SERVER_HPP
#define SERVER_HPP

#include <cpprest/filestream.h>
#include <cpprest/http_listener.h>              // HTTP server
#include <cpprest/json.h>                       // JSON library
#include <cpprest/uri.h>                        // URI library
#include <cpprest/ws_client.h>                  // WebSocket client
#include <cpprest/containerstream.h>            // Async streams backed by STL containers
#include <cpprest/interopstream.h>              // Bridges for integrating Async streams with STL and WinRT streams
#include <cpprest/rawptrstream.h>               // Async streams backed by raw pointer to memory
#include <cpprest/producerconsumerstream.h>     // Async streams for producer consumer scenarios
#include <cpprest/http_msg.h>


using namespace std;
using namespace web;
using namespace http;
using namespace utility;
using namespace web::http;
using namespace http::experimental::listener;



class Server
{
private:
 
    http_listener m_listener;

    std::map<utility::string_t, utility::string_t> param;

    void handle_get(http_request message);
  
    void handle_post(http_request message);
    
    void handle_put(http_request message);

    void handle_del(http_request message);
    
    
public:

    Server() : m_listener(U("http://0.0.0.0:4200/api"))
    {
      
        m_listener.support(methods::GET, bind(&Server::handle_get, this, placeholders::_1));
        m_listener.support(methods::POST, bind(&Server::handle_post, this, placeholders::_1));
        m_listener.support(methods::PUT, bind(&Server::handle_put, this, placeholders::_1));
        m_listener.support(methods::DEL, bind(&Server::handle_del, this, placeholders::_1));
    }

    pplx::task<void> open() { return m_listener.open();}
    pplx::task<void> close() { return m_listener.close(); }
    

    std::string getEndpoint() const;

    void set_parameters_map(std::map<std::string, std::string>  mp);
 

};

#endif  //SERVER_HPP
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdio.h>
#include "server.hpp"
#include "multipart_parser.hpp"

// LL: utility functions
void display_json(json::value const & jvalue, utility::string_t const & prefix)
{
   std::cout << prefix << jvalue.serialize() << std::endl;
}

void handle_request(http_request request,function<void(json::value const &, json::value &)> action)
{
   auto answer = json::value::object();
 
   request.extract_json().then([&answer, &action](pplx::task<json::value> task) {
         try
         {
            auto const & jvalue = task.get();
            display_json(jvalue, U("R: "));
 
            if (!jvalue.is_null())
            {
               action(jvalue, answer);
            }
         }
         catch (http_exception const & e)
         {
            std::cout << e.what() << std::endl;
         }
      }).wait();
 
   
   display_json(answer, U("S: "));
 
   request.reply(status_codes::OK, answer);
}

std::string Server::getEndpoint() const
{
    return m_listener.uri().to_string();
}



void Server::set_parameters_map(std::map<std::string, std::string>  mp)
{ 
    std::map<utility::string_t, utility::string_t> map_temp;
    
    for (auto const & p : mp)
    {
       map_temp[utility::conversions::to_string_t(p.first)] = utility::conversions::to_string_t(p.second);
    }
    param = map_temp;
}

void Server::handle_get(http_request message)
{


auto path = uri::split_path(uri::decode(message.relative_uri().path()));
std::cout<< "  "<<path.size() << " " << path[0] <<"  "<< std::endl;

 if (!path.empty()) {

    if (path.size() == 1 && path[0] == "parameters") 
    {

     std::cout << "Method: " << message.method() << std::endl;
     std::cout << "URI: " << http::uri::decode(message.relative_uri().path()) << std::endl;
     std::cout << "Query: " << http::uri::decode(message.relative_uri().query()) << std::endl << std::endl;
    
     auto answer = json::value::object();
 
     for (auto const & p : param)
     {
        answer[p.first] = json::value::string(p.second);
     }

     message.reply(status_codes::OK, answer);

    } 
    else if (path.size() == 1 && path[0] == "download") 
    {
     std::cout << "Method: " << message.method() << std::endl;
     std::cout << "URI: " << http::uri::decode(message.relative_uri().path()) << std::endl;
     std::cout << "Query: " << http::uri::decode(message.relative_uri().query()) << std::endl << std::endl;
    
     auto fileBuffer = std::make_shared<Concurrency::streams::basic_ostream<uint8_t>>();

    try
    {
     pplx::task<void> requestTask = Concurrency::streams::fstream::open_ostream(U("results")).then([=](Concurrency::streams::ostream outFile)
      {
        *fileBuffer = outFile;

        //Use MultipartParser to get the encoded body content and boundary
        MultipartParser parser;
        parser.AddParameter("Filename", "uploaded.obj");
        parser.AddFile("file", "uploaded.obj");
        std::string boundary = parser.boundary();
        std::string body = parser.GenBodyContent();

        http_response resp(status_codes::OK);

        //Set up http client and request
        resp.set_body(body, "multipart/form-data; boundary=" + boundary);
        return message.reply(resp);

    }).then([=](pplx::task<void> Previous_task)
    {
        fileBuffer->close();
    }).then([](pplx::task<void> previousTask)
    {
        // This continuation is run because it is value-based.
        try
        {
            // The call to task::get rethrows the exception.
        previousTask.get();
        }
        catch (const exception& e)
        {
            std::cout << e.what() << std::endl;

        }
        });

    

    }
    catch (const exception& e)
    {
        wcout << e.what() << endl;
    }



    }
    else if (path.size() == 1 && path[0] == "home"  ) {

     std::cout << "Method: " << message.method() << std::endl;
     std::cout << "URI: " << http::uri::decode(message.relative_uri().path()) << std::endl;
     std::cout << "Query: " << http::uri::decode(message.relative_uri().query()) << std::endl << std::endl;
     

    concurrency::streams::fstream::open_istream(U("static/index.html"), std::ios::in).then([=](concurrency::streams::istream is)
    {
        message.reply(status_codes::OK, is,  U("text/html")).then([](pplx::task<void> t)
		{
			try{
				t.get();
			}
			catch(...){
				//
			}
	    });
    }).then([=](pplx::task<void>t)
	{
		try{
			t.get();
		}
		catch(...){
			message.reply(status_codes::InternalError,U("INTERNAL ERROR "));
		}
	});

    } 
    else {
        message.reply(status_codes::NotFound);
    }
 }
else 
{
    message.reply(status_codes::NotFound);
}
}

void Server::handle_post(http_request message)
{
    auto path = uri::split_path(uri::decode(message.relative_uri().path()));
    
    std::string line;
    std::string boundary;
    std::string disp; /* content disposition string */
    std::string type; /* content type string */
    std::string file; /* actual file content */
	
    if (!path.empty())
    {
        
    if ( path.size() == 1 && path[0] == "upload") {
     std::remove("uploaded.obj");
     std::cout << "Method: " << message.method() << std::endl;
     std::cout << "URI: " << http::uri::decode(message.relative_uri().path()) << std::endl;
     std::cout << "Query: " << http::uri::decode(message.relative_uri().query()) << std::endl << std::endl;

     auto fileBuffer = std::make_shared<Concurrency::streams::basic_ostream<uint8_t>>();

     try
        {
        auto stream = Concurrency::streams::fstream::open_ostream(U("uploaded.obj"),std::ios_base::out | std::ios_base::binary)
                      .then([message, fileBuffer](pplx::task<Concurrency::streams::basic_ostream<unsigned char>> Previous_task)
        {
            
            *fileBuffer = Previous_task.get();
            try
            {
                message.body().read_to_end(fileBuffer->streambuf()).get();
            }
            catch (const exception&)
            {
                std::cout << L"<exception>" << std::endl;

            }

        })
        .then([=](pplx::task<void> Previous_task)
        {


        fileBuffer->close();
        //Previous_task.get();
        }).then([](pplx::task<void> previousTask)
        {
        // This continuation is run because it is value-based.
            try
            {
            // The call to task::get rethrows the exception.

                previousTask.get();
            }
            catch (const exception& e)
            {
                std::cout << e.what() << std::endl;

            }
        });
    //stream.get().close();
        }
    catch (const exception& e)
    {
        wcout << e.what() << endl;
    }
    http_response resp(status_codes::OK);

    message.reply(resp);

    }
    else if ( path.size() == 1 && path[0] == "process" ){
        
    }
    else
    {
        message.reply(status_codes::NotFound);
    }
    }
    else 
    {
        message.reply(status_codes::NotFound);
    }
}

void Server::handle_put(http_request message)
{
    auto path = uri::split_path(uri::decode(message.relative_uri().path()));

    if (!path.empty()) 
    {
     if ( path.size() == 1 && path[0] == "modify") {

     handle_request(message,[this](json::value const & jvalue, json::value & answer)
     {
      for (auto const & e : jvalue.as_object())
      {
         if (e.second.is_string())
         {
            auto key = e.first;
            auto value = e.second.as_string();
 
            if (param.find(key) == param.end())
            {
               answer[key] = json::value::string(U("<put>"));
            }
            else
            {

               answer[key] = json::value::string(U("<updated>"));
            }
 
            param[key] = value;
         }
      }
     });
 
    }
    else 
    {
        message.reply(status_codes::NotFound);
    }
    }
    else 
    {
        message.reply(status_codes::NotFound);
    }
    
}

void Server::handle_del(http_request message)
{
    message.reply(status_codes::OK, U("Hello world!"));
}
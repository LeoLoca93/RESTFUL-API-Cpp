#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <cpprest/json.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "multipart_parser.hpp"


using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams

void display_json(json::value const & jvalue, utility::string_t const & prefix)
{
   std::cout << prefix << jvalue.serialize() << std::endl;
}
 
pplx::task<http_response> make_task_request(http_client & client,method mtd,json::value const & jvalue)
{
   return (mtd == methods::GET || mtd == methods::HEAD) ?
      client.request(mtd, U("/parameters")) :
      client.request(mtd, U("/modify"), jvalue);
}
 
void make_request(http_client & client, method mtd, json::value const & jvalue)
{
   make_task_request(client, mtd, jvalue)
      .then([](http_response response)
      {
         if (response.status_code() == status_codes::OK)
         {
            return response.extract_json();
         }
         return pplx::task_from_result(json::value());
      })
      .then([](pplx::task<json::value> previousTask)
      {
         try
         {
            display_json(previousTask.get(), U("R: "));
         }
         catch (http_exception const & e)
         {
            std::cout << e.what() << std::endl;
         }
      })
      .wait();
}

int main(int argc, char* argv[])
{

   // LL: test POST upload
    auto fileStream = std::make_shared<ostream>();

    // Open stream to output file.
    pplx::task<void> requestTask = fstream::open_ostream(U("results")).then([=](ostream outFile)
    {
        *fileStream = outFile;

        //Use MultipartParser to get the encoded body content and boundary
        MultipartParser parser;
        parser.AddParameter("Filename", "untitled2obj.obj");
        parser.AddFile("file", "untitled2obj.obj");
        std::string boundary = parser.boundary();
        std::string body = parser.GenBodyContent();
        //std::cout << body << std::endl;

        //Set up http client and request
        http_request req;
        http_client client(U("http://0.0.0.0:4200/api/upload"));
        req.set_method(web::http::methods::POST);
        req.set_body(body, "multipart/form-data; boundary=" + boundary);
        return client.request(req);
    })
    .then([=](pplx::task<http_response> response_task)
    {
        http_response response = response_task.get();
        return response.body().read_to_end(fileStream->streambuf());
    })
    .then([=](size_t)
    {
        return fileStream->close();
    });

    // Wait for all the outstanding I/O to complete and handle any exceptions
    try
    {
        requestTask.wait();
    }
    catch (const std::exception &e)
    {
        printf("Error exception:%s\n", e.what());
    }
    

    http_client client_g(U("http://0.0.0.0:4200/api"));

    auto nullvalue = json::value::null();
    
   // LL: test get parameters
    std::cout << U("\nGET (get all values)\n");
    display_json(nullvalue, U("S: "));
    make_request(client_g, methods::GET, nullvalue);
   
   // LL: test put parameters
    auto putvalue = json::value::object(); 
    auto putvalue2 = json::value::object();
    putvalue[U("one")] = json::value::string(U("100"));
    putvalue2[U("radius")] = json::value::string(U("200"));
    std::cout << U("\nput values\n");
    make_request(client_g, methods::PUT, putvalue);
    make_request(client_g, methods::PUT, putvalue2);

   // LL: Test get parameters
    std::cout << U("\nGET (get all values new)\n");
    display_json(nullvalue, U("S: "));
    make_request(client_g, methods::GET, nullvalue);

   // LL: Test download 
   client_g.request(methods::GET, U("/download")).then([](http_response response)
   {
      std::remove("uploaded.obj");
      auto fileBuffer = std::make_shared<Concurrency::streams::basic_ostream<uint8_t>>();
      
      auto stream = Concurrency::streams::fstream::open_ostream(U("uploaded.obj"),std::ios_base::out | std::ios_base::binary)
                      .then([response, fileBuffer](pplx::task<Concurrency::streams::basic_ostream<unsigned char>> Previous_task)
        {
 
            *fileBuffer = Previous_task.get();
            try
            {
                response.body().read_to_end(fileBuffer->streambuf()).get();
                std::cout<< response.body();
            }
            catch (http_exception const & e)
            {
                std::cout << L"<exception>" << std::endl;
            //return pplx::task_from_result();
            }
        //Previous_task.get().close();

        })
        .then([=](pplx::task<void> Previous_task)
        {


        fileBuffer->close();
        //Previous_task.get();
        }).then([](pplx::task<void> previousTask)
        {
        // This continuation is run because it is value-based.
            try
            {
            // The call to task::get rethrows the exception.

                previousTask.get();
            }
            catch (http_exception const & e)
            {
                std::cout << e.what() << std::endl;

            }
        });
   }
   );

   


    return 0;
}
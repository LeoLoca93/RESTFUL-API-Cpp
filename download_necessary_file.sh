#!/bin/bash

# googletest 
git clone https://github.com/google/googletest.git googletest && cd googletest && cmake . && make && cd ..

# getpot
curl -o getpot-c%2B%2B-2.0.tgz -L https://sourceforge.net/projects/getpot/files/DOWNLOAD/getpot-c%2B%2B-2.0.tgz && tar zxf getpot-c%2B%2B-2.0.tgz 

# mmg
curl -o mmg-5.4.3-Linux-4.4.0-170-generic-appli.tar.gz -L https://www.mmgtools.org/files/2020/02/mmg-5.4.3-Linux-4.4.0-170-generic-appli.tar.gz && mkdir mmg_executable && tar zxf mmg-5.4.3-Linux-4.4.0-170-generic-appli.tar.gz -C mmg_executable 

# boost 1.70.0
curl -o boost_1_70_0.tar.gz -L https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.gz && tar zxf boost_1_70_0.tar.gz  

# octree
git clone https://github.com/jbehley/octree.git octree

# libpng
curl -o libpng-1.6.37.tar.gz -L https://sourceforge.net/projects/libpng/files/libpng16/1.6.37/libpng-1.6.37.tar.gz && tar zxf libpng-1.6.37.tar.gz 

# cmake modules
git clone https://github.com/jedbrown/cmake-modules.git cmake-modules

# omega_h   
git clone https://github.com/SNLComputation/omega_h.git omega_h && mkdir Config-Omega_h && mv do-config-omega_h.sh Config-Omega_h && cd Config-Omega_h && sudo PETSC_DIR=/usr/local/petsc-32 SLEPC_DIR=/usr/local/slepc-32 ./do-config-omega_h.sh && sudo make install && cd .. 
